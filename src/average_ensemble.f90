program average_ensemble
!
!   compute the ensemble average of a set of time series
!
    implicit none
    include 'param.inc'
    integer :: i,mo,yr,iens,mens1,mens,nperyear,iarg,nens1,nens2,mensmax
    integer :: fyr,lyr,ffyr,llyr,nmissing(yrbeg:yrend),nnn(yrbeg:yrend),n0missing,n0,nlength(10)
    integer,allocatable :: nn(:,:)
    real,allocatable :: data(:,:,:),mean(:,:)
    logical :: lstandardunits,lset,lwrite
    character :: file*1023,var*40,units*120,string*80,oper*3,command*500
    character,allocatable :: ids(:)*30
    character :: lvar*120,svar*120,history*50000,metadata(2,100)*2000
    integer,external :: leap

    if ( command_argument_count() < 2 ) then
        print *,'usage: average_ensemble ensfile|(file setfile) [ens n1 n2] '// &
            '[mean|min|max|num] [debug] [dummy]'
        call exit(-1)
    endif

    lstandardunits = .false.
    lwrite = .false.
    call get_command_argument(1,file)
    if ( file == 'file' ) then
        call get_command_argument(2,file)
        lset = .true.
        iarg = 4
        mensmax = 15000
        fyr = 1500
        lyr = 2100
    else
        lset = .false.
        iarg = 2
        mensmax = 3999
        fyr = yrbeg
        lyr = yrend
    end if

    allocate(data(1:npermax,fyr:lyr,0:mensmax))
    allocate(mean(1:npermax,fyr:lyr))
    allocate(nn(1:npermax,fyr:lyr))
    allocate(ids(0:mensmax))

    nens1 = 0
    nens2 = mensmax
    oper = 'mea'
    do while ( iarg <= command_argument_count() )
        call get_command_argument(iarg,string)
        if ( string == 'ens' ) then
            call get_command_argument(iarg+1,string)
            read(string,*) nens1
            call get_command_argument(iarg+2,string)
            read(string,*) nens2
            iarg = iarg + 3
        else if ( string == 'mean' ) then
            oper = 'mea'
            iarg = iarg + 1
        else if ( string == 'min' ) then
            oper = 'min'
            iarg = iarg + 1
        else if ( string == 'max' ) then
            oper = 'max'
            iarg = iarg + 1
        else if ( string == 'num' ) then
            oper = 'num'
            iarg = iarg + 1
        else if ( string == 'mis' ) then
            oper = 'mis'
            iarg = iarg + 1
        else if ( string == 'debug' .or. string == 'lwrite' ) then
            lwrite = .true.
            print *,'turned on debug output'
            iarg = iarg + 1
        else
            iarg = iarg + 1
        end if
    end do
    if ( lset ) then
        call readsetseriesmeta(data,ids,npermax,fyr,lyr,mensmax    &
            ,nperyear,mens1,mens,var,units,lvar,svar,history,metadata,lstandardunits,lwrite)
    else
        call readensseriesmeta(trim(file),data,npermax,fyr,lyr,mensmax   &
            ,nperyear,mens1,mens,var,units,lvar,svar,history,metadata,lstandardunits,lwrite)
    end if
    nens1 = max(nens1,mens1)
    nens2 = min(nens2,mens)
    if ( nperyear < 1 .or. nperyear > 24*366 ) then
        write(0,*) 'average_ensemble: cannot handle nperyear = ',nperyear
        call exit(-1)
    end if

    nn(1:nperyear,fyr:lyr) = 0
    if ( oper == 'mea' .or. oper == 'num' ) then
        mean(1:nperyear,fyr:lyr) = 0
    else if ( oper == 'min' ) then
        mean(1:nperyear,fyr:lyr) = 3e33
    else if ( oper == 'max' ) then
        mean(1:nperyear,fyr:lyr) = -3e33
    else if ( oper == 'mis' ) then
        ffyr = fyr
        llyr = lyr
        call getmissing(data,npermax,ffyr,llyr,nens1,nens2, &
            nperyear,1,nperyear,fyr,lyr,nmissing,nnn,n0missing,n0,nlength)
    else
        write(0,*) 'average_ensemble: error: unknown operation ',oper
        call exit(-1)
    end if
    if ( oper /= 'mis' ) then
        do iens=nens1,nens2
            do yr=fyr,lyr
                do mo=1,nperyear
                    if ( data(mo,yr,iens) < 1e33 ) then
                        nn(mo,yr) = nn(mo,yr) + 1
                        if ( oper == 'mea' ) then
                            mean(mo,yr) = mean(mo,yr) + data(mo,yr,iens)
                        else if ( oper == 'min' ) then
                            mean(mo,yr) = min(mean(mo,yr),data(mo,yr,iens))
                        else if ( oper == 'max' ) then
                            mean(mo,yr) = max(mean(mo,yr),data(mo,yr,iens))
                        else if ( oper == 'num' ) then
                            mean(mo,yr) = mean(mo,yr) + 1
                        else
                            write(0,*) 'average_ensemble: error: '//    &
                                 'unknown operation 2 ',oper
                            call exit(-1)
                        end if
                    endif
                enddo
            enddo
        enddo
        if ( oper == 'num' ) then
            do ffyr=fyr,lyr
                do mo=1,nperyear
                    if ( nn(mo,ffyr) > 0 ) goto 110
                end do
            end do
110         continue
            do llyr=lyr,fyr,-1
                do mo=1,nperyear
                    if ( nn(mo,llyr) > 0 ) goto 120
                end do
            end do
120         continue
            if ( ffyr > fyr ) mean(:,fyr:ffyr-1) = 3e33
            if ( llyr < lyr ) mean(:,llyr+1:lyr) = 3e33
            if ( nperyear == 366 ) then
                do yr=ffyr,llyr
                    if ( leap(yr) == 1 ) then
                        if ( mean(60,yr) == 0 ) then
                            mean(60,yr) = 3e33
                        else
                            write(0,*) 'average_ensemble: suspicious'//    &
                                 ' value on 29 Feb ',yr,mean(60,yr)
                        end if
                    end if
                end do
            end if
        else
            do yr=fyr,lyr
                do mo=1,nperyear
                    if ( nn(mo,yr) > 1 ) then
                        if ( oper == 'mea' ) then
                            mean(mo,yr) = mean(mo,yr)/nn(mo,yr)
                        end if
                    else
                        mean(mo,yr) = 3e33
                    endif
                enddo
            enddo
        end if
    else ! oper == 'mis'
        nperyear = 1
        mean = 3e33
        do yr=fyr,lyr
            if ( nnn(yr) > 0 ) then
                mean(1,yr) = real(nmissing(yr))/real(nnn(yr))
            end if
        end do
    end if
    print '(4a)','# ensemble ',oper,' of file ',trim(file)
    print '(a,i3,a,i3)','# using members ',nens1,' to ',nens2
    if ( oper == 'num' ) then
        print '(a)','# N [1] number of series with valid data'
    else if ( oper == 'mis' ) then
        print '(a)','# miss [1] fraction with invalid data'
    else
        call printvar(6,var,units,lvar)
    end if
    call printmetadata(6,file," "," ",history,metadata)
    call printdatfile(6,mean,npermax,nperyear,fyr,lyr)

end program average_ensemble
